// feedback style changing trigger
$(".content_list li a").click(function () {
    if (this.id == "feedback_trigger") {
        $(".feedback").addClass("fb_design");
    } else {
        $(".feedback").removeClass("fb_design");
        $(".right_feedback_content").removeClass("right_feedback_content_show");
    }
});



// header color start
$(document).ready(function () {
    // for green
    $(".theme_color #green").click(function () {
        $(".fb_design .content").addClass("fb_green");
        $(".fb_design .content").removeClass("fb_blue");
        $(".fb_design .content").removeClass("fb_orange");
        $(".fb_design .content").removeClass("fb_red");
        $(".fb_design .content").removeClass("fb_yellow");
        $(".fb_design .content").removeClass("fb_lime");
        $(".fb_design .content").removeClass("fb_turquoise");
        $(".fb_design .content").removeClass("fb_pink");
        $(".fb_design .content").removeClass("fb_magenta");
        $(".fb_design .content").removeClass("fb_purple");
        $(".fb_design .content").removeClass("fb_cream");
        $(".fb_design .content").removeClass("fb_chocolate");
        $(".fb_design .content").removeClass("fb_silver");
        $(".fb_design .content").removeClass("fb_cobalt");
    });
    // for blue
    $(".theme_color #blue").click(function () {
        $(".fb_design .content").addClass("fb_blue");
        $(".fb_design .content").removeClass("fb_green");
        $(".fb_design .content").removeClass("fb_orange");
        $(".fb_design .content").removeClass("fb_red");
        $(".fb_design .content").removeClass("fb_yellow");
        $(".fb_design .content").removeClass("fb_lime");
        $(".fb_design .content").removeClass("fb_turquoise");
        $(".fb_design .content").removeClass("fb_pink");
        $(".fb_design .content").removeClass("fb_magenta");
        $(".fb_design .content").removeClass("fb_purple");
        $(".fb_design .content").removeClass("fb_cream");
        $(".fb_design .content").removeClass("fb_chocolate");
        $(".fb_design .content").removeClass("fb_silver");
        $(".fb_design .content").removeClass("fb_cobalt");
    });
    // for orange
    $(".theme_color #orange").click(function () {
        $(".fb_design .content").addClass("fb_orange");
        $(".fb_design .content").removeClass("fb_green");
        $(".fb_design .content").removeClass("fb_blue");
        $(".fb_design .content").removeClass("fb_red");
        $(".fb_design .content").removeClass("fb_yellow");
        $(".fb_design .content").removeClass("fb_lime");
        $(".fb_design .content").removeClass("fb_turquoise");
        $(".fb_design .content").removeClass("fb_pink");
        $(".fb_design .content").removeClass("fb_magenta");
        $(".fb_design .content").removeClass("fb_purple");
        $(".fb_design .content").removeClass("fb_cream");
        $(".fb_design .content").removeClass("fb_chocolate");
        $(".fb_design .content").removeClass("fb_silver");
        $(".fb_design .content").removeClass("fb_cobalt");
    });
    // for red
    $(".theme_color #red").click(function () {
        $(".fb_design .content").addClass("fb_red");
        $(".fb_design .content").removeClass("fb_green");
        $(".fb_design .content").removeClass("fb_blue");
        $(".fb_design .content").removeClass("fb_orange");
        $(".fb_design .content").removeClass("fb_yellow");
        $(".fb_design .content").removeClass("fb_lime");
        $(".fb_design .content").removeClass("fb_turquoise");
        $(".fb_design .content").removeClass("fb_pink");
        $(".fb_design .content").removeClass("fb_magenta");
        $(".fb_design .content").removeClass("fb_purple");
        $(".fb_design .content").removeClass("fb_cream");
        $(".fb_design .content").removeClass("fb_chocolate");
        $(".fb_design .content").removeClass("fb_silver");
        $(".fb_design .content").removeClass("fb_cobalt");
    });
    // for yellow
    $(".theme_color #yellow").click(function () {
        $(".fb_design .content").addClass("fb_yellow");
        $(".fb_design .content").removeClass("fb_green");
        $(".fb_design .content").removeClass("fb_blue");
        $(".fb_design .content").removeClass("fb_orange");
        $(".fb_design .content").removeClass("fb_red");
        $(".fb_design .content").removeClass("fb_lime");
        $(".fb_design .content").removeClass("fb_turquoise");
        $(".fb_design .content").removeClass("fb_pink");
        $(".fb_design .content").removeClass("fb_magenta");
        $(".fb_design .content").removeClass("fb_purple");
        $(".fb_design .content").removeClass("fb_cream");
        $(".fb_design .content").removeClass("fb_chocolate");
        $(".fb_design .content").removeClass("fb_silver");
        $(".fb_design .content").removeClass("fb_cobalt");
    });
    // for lime
    $(".theme_color #lime").click(function () {
        $(".fb_design .content").addClass("fb_lime");
        $(".fb_design .content").removeClass("fb_green");
        $(".fb_design .content").removeClass("fb_blue");
        $(".fb_design .content").removeClass("fb_orange");
        $(".fb_design .content").removeClass("fb_red");
        $(".fb_design .content").removeClass("fb_yellow");
        $(".fb_design .content").removeClass("fb_turquoise");
        $(".fb_design .content").removeClass("fb_pink");
        $(".fb_design .content").removeClass("fb_magenta");
        $(".fb_design .content").removeClass("fb_purple");
        $(".fb_design .content").removeClass("fb_cream");
        $(".fb_design .content").removeClass("fb_chocolate");
        $(".fb_design .content").removeClass("fb_silver");
        $(".fb_design .content").removeClass("fb_cobalt");
    });
    // for turquoise
    $(".theme_color #turquoise").click(function () {
        $(".fb_design .content").addClass("fb_turquoise");
        $(".fb_design .content").removeClass("fb_green");
        $(".fb_design .content").removeClass("fb_blue");
        $(".fb_design .content").removeClass("fb_orange");
        $(".fb_design .content").removeClass("fb_red");
        $(".fb_design .content").removeClass("fb_yellow");
        $(".fb_design .content").removeClass("fb_lime");
        $(".fb_design .content").removeClass("fb_pink");
        $(".fb_design .content").removeClass("fb_magenta");
        $(".fb_design .content").removeClass("fb_purple");
        $(".fb_design .content").removeClass("fb_cream");
        $(".fb_design .content").removeClass("fb_chocolate");
        $(".fb_design .content").removeClass("fb_silver");
        $(".fb_design .content").removeClass("fb_cobalt");
    });
    // for pink
    $(".theme_color #pink").click(function () {
        $(".fb_design .content").addClass("fb_pink");
        $(".fb_design .content").removeClass("fb_green");
        $(".fb_design .content").removeClass("fb_blue");
        $(".fb_design .content").removeClass("fb_orange");
        $(".fb_design .content").removeClass("fb_red");
        $(".fb_design .content").removeClass("fb_yellow");
        $(".fb_design .content").removeClass("fb_lime");
        $(".fb_design .content").removeClass("fb_turquoise");
        $(".fb_design .content").removeClass("fb_magenta");
        $(".fb_design .content").removeClass("fb_purple");
        $(".fb_design .content").removeClass("fb_cream");
        $(".fb_design .content").removeClass("fb_chocolate");
        $(".fb_design .content").removeClass("fb_silver");
        $(".fb_design .content").removeClass("fb_cobalt");
    });
    // for magenta
    $(".theme_color #magenta").click(function () {
        $(".fb_design .content").addClass("fb_magenta");
        $(".fb_design .content").removeClass("fb_green");
        $(".fb_design .content").removeClass("fb_blue");
        $(".fb_design .content").removeClass("fb_orange");
        $(".fb_design .content").removeClass("fb_red");
        $(".fb_design .content").removeClass("fb_yellow");
        $(".fb_design .content").removeClass("fb_lime");
        $(".fb_design .content").removeClass("fb_turquoise");
        $(".fb_design .content").removeClass("fb_pink");
        $(".fb_design .content").removeClass("fb_purple");
        $(".fb_design .content").removeClass("fb_cream");
        $(".fb_design .content").removeClass("fb_chocolate");
        $(".fb_design .content").removeClass("fb_silver");
        $(".fb_design .content").removeClass("fb_cobalt");
    });
    // for purple
    $(".theme_color #purple").click(function () {
        $(".fb_design .content").addClass("fb_purple");
        $(".fb_design .content").removeClass("fb_green");
        $(".fb_design .content").removeClass("fb_blue");
        $(".fb_design .content").removeClass("fb_orange");
        $(".fb_design .content").removeClass("fb_red");
        $(".fb_design .content").removeClass("fb_yellow");
        $(".fb_design .content").removeClass("fb_lime");
        $(".fb_design .content").removeClass("fb_turquoise");
        $(".fb_design .content").removeClass("fb_pink");
        $(".fb_design .content").removeClass("fb_magenta");
        $(".fb_design .content").removeClass("fb_cream");
        $(".fb_design .content").removeClass("fb_chocolate");
        $(".fb_design .content").removeClass("fb_silver");
        $(".fb_design .content").removeClass("fb_cobalt");
    });
    // for cream
    $(".theme_color #cream").click(function () {
        $(".fb_design .content").addClass("fb_cream");
        $(".fb_design .content").removeClass("fb_green");
        $(".fb_design .content").removeClass("fb_blue");
        $(".fb_design .content").removeClass("fb_orange");
        $(".fb_design .content").removeClass("fb_red");
        $(".fb_design .content").removeClass("fb_yellow");
        $(".fb_design .content").removeClass("fb_lime");
        $(".fb_design .content").removeClass("fb_turquoise");
        $(".fb_design .content").removeClass("fb_pink");
        $(".fb_design .content").removeClass("fb_magenta");
        $(".fb_design .content").removeClass("fb_purple");
        $(".fb_design .content").removeClass("fb_chocolate");
        $(".fb_design .content").removeClass("fb_silver");
        $(".fb_design .content").removeClass("fb_cobalt");
    });
    // for chocolate
    $(".theme_color #chocolate").click(function () {
        $(".fb_design .content").addClass("fb_chocolate");
        $(".fb_design .content").removeClass("fb_green");
        $(".fb_design .content").removeClass("fb_blue");
        $(".fb_design .content").removeClass("fb_orange");
        $(".fb_design .content").removeClass("fb_red");
        $(".fb_design .content").removeClass("fb_yellow");
        $(".fb_design .content").removeClass("fb_lime");
        $(".fb_design .content").removeClass("fb_turquoise");
        $(".fb_design .content").removeClass("fb_pink");
        $(".fb_design .content").removeClass("fb_magenta");
        $(".fb_design .content").removeClass("fb_purple");
        $(".fb_design .content").removeClass("fb_cream");
        $(".fb_design .content").removeClass("fb_silver");
        $(".fb_design .content").removeClass("fb_cobalt");
    });
    // for silver
    $(".theme_color #silver").click(function () {
        $(".fb_design .content").addClass("fb_silver");
        $(".fb_design .content").removeClass("fb_green");
        $(".fb_design .content").removeClass("fb_blue");
        $(".fb_design .content").removeClass("fb_orange");
        $(".fb_design .content").removeClass("fb_red");
        $(".fb_design .content").removeClass("fb_yellow");
        $(".fb_design .content").removeClass("fb_lime");
        $(".fb_design .content").removeClass("fb_turquoise");
        $(".fb_design .content").removeClass("fb_pink");
        $(".fb_design .content").removeClass("fb_magenta");
        $(".fb_design .content").removeClass("fb_purple");
        $(".fb_design .content").removeClass("fb_cream");
        $(".fb_design .content").removeClass("fb_chocolate");
        $(".fb_design .content").removeClass("fb_cobalt");
    });
    // for cobalt
    $(".theme_color #cobalt").click(function () {
        $(".fb_design .content").addClass("fb_cobalt");
        $(".fb_design .content").removeClass("fb_green");
        $(".fb_design .content").removeClass("fb_blue");
        $(".fb_design .content").removeClass("fb_orange");
        $(".fb_design .content").removeClass("fb_red");
        $(".fb_design .content").removeClass("fb_yellow");
        $(".fb_design .content").removeClass("fb_lime");
        $(".fb_design .content").removeClass("fb_turquoise");
        $(".fb_design .content").removeClass("fb_pink");
        $(".fb_design .content").removeClass("fb_magenta");
        $(".fb_design .content").removeClass("fb_purple");
        $(".fb_design .content").removeClass("fb_cream");
        $(".fb_design .content").removeClass("fb_chocolate");
        $(".fb_design .content").removeClass("fb_silver");
    });
});
// header color end 


// text color start 
// $(document).ready(function () {
//     // for text light 
//     $(".text_color #text_light").click(function () {
//         $(".feedback .content").addClass("fb_text_light");
//         $(".feedback .content").removeClass("fb_text_dark");
//     });
//     // for text dark 
//     $(".text_color #text_dark").click(function () {
//         $(".feedback .content").addClass("fb_text_dark");
//         $(".feedback .content").removeClass("fb_text_light");
//     });
// });