// cookie trigger
$(document).ready(function () {
    $("#cookie_trigger").click(function () {
        $(".cookie_popup").addClass("cookie_hide");
    });
});

// design_aside_area filter
$(document).ready(function () {
    $("#design_asideToggle").click(function () {
        $("#design_aside").toggle();
    });
});


// bookmark filter 
$(document).ready(function () {
    $("#filterToggle").click(function () {
        $("#filter-box").toggle();
    });
});

// list/grid view
$(document).ready(function () {
    $('#list_trigger').click(function (event) { event.preventDefault(); $('.blog_content_area .grid').removeClass('col-md-6 col-md-4'); $('.blog_content_area .grid').addClass('col-md-12 list_view'); });
    
    $('#grid_sm_trigger').click(function (event) { event.preventDefault(); $('.blog_content_area .grid').removeClass('col-md-4 col-md-12 list_view'); $('.blog_content_area .grid').addClass('col-md-6'); });

    $('#grid_lg_trigger').click(function (event) { event.preventDefault(); $('.blog_content_area .grid').removeClass('col-md-6 col-md-12 list_view'); $('.blog_content_area .grid').addClass('col-md-4'); });

});


// feedback 
$(document).ready(function () {
    $("#feedback").click(function () {
        $(".right_feedback_content").addClass("right_feedback_content_show");
    });
    $(".feedback_close").click(function () {
        $(".right_feedback_content").removeClass("right_feedback_content_show");
    });
});

// message 
$(document).ready(function () {
    $("#message").click(function () {
        $(".message_content").addClass("message_content_show");
    });
    $(".message_close").click(function () {
        $(".message_content").removeClass("message_content_show");
    });
});


// // feedback_bottom
$(window).scroll(function () {
    if ($(this).scrollTop() > 150) {
        var condition = $('.feedback_bottom').hasClass('closed_once');        
        if (condition) {
            return false;
        } else {
            $('.feedback_bottom').addClass('feedback_bottom_show');
        }
    } else {
        $('.feedback_bottom').removeClass('feedback_bottom_show');
    }
    $(".feedback_bottom_close").click(function () {
        $(".feedback_bottom").removeClass("feedback_bottom_show");
        $(".feedback_bottom").addClass("closed_once");
    });
});


// signin-signup modal 
$(document).ready(function () {
    $(".signup_form_trigger").click(function () {
        $(".signup_area").removeClass("active");
        $(".signup_form").addClass("active");
    });
    $(".signin_trigger").click(function () {
        $(".signup_area").removeClass("active");
        $(".signup_form").removeClass("active");
        $(".signin_trigger_area").removeClass("active");
        $(".description").removeClass("active");
        $(".signup_headline").removeClass("active");
        $(".signin_form").addClass("active");
        $(".signup_trigger_area").addClass("active");
        $(".signin_headline").addClass("active");
    });
    $(".signup_trigger").click(function () {
        $(".signup_area").addClass("active");
        $(".signup_form").removeClass("active");
        $(".signin_trigger_area").addClass("active");
        $(".description").addClass("active");
        $(".signup_headline").addClass("active");
        $(".signin_form").removeClass("active");
        $(".signup_trigger_area").removeClass("active");
        $(".signin_headline").removeClass("active");
    });
});




// typeform
(
function () {
    var qs, js, q, s, d = document, gi = d.getElementById, ce = d.createElement, gt = d.getElementsByTagName, id = "typef_orm", b = "https://embed.typeform.com/"; if (!gi.call(d, id)) { js = ce.call(d, "script"); js.id = id; js.src = b + "embed.js"; q = gt.call(d, "script")[0]; q.parentNode.insertBefore(js, q) }

})()