// feedback style changing trigger
$(".content_list li a").click(function () {
    if (this.id == "chat_trigger") {
        $(".message_section").addClass("chat_design");
        $(".message_content").addClass("message_content_show");
    } else {
        $(".message_section").removeClass("chat_design");
        $(".message_content").removeClass("message_content_show");
    }
});



// header color start
$(document).ready(function () {
    // for green
    $(".theme_color #green").click(function () {
        $(".chat_design .main_content").addClass("chat_green");
        $(".chat_design .main_content").removeClass("chat_blue");
        $(".chat_design .main_content").removeClass("chat_orange");
        $(".chat_design .main_content").removeClass("chat_red");
        $(".chat_design .main_content").removeClass("chat_yellow");
        $(".chat_design .main_content").removeClass("chat_lime");
        $(".chat_design .main_content").removeClass("chat_turquoise");
        $(".chat_design .main_content").removeClass("chat_pink");
        $(".chat_design .main_content").removeClass("chat_magenta");
        $(".chat_design .main_content").removeClass("chat_purple");
        $(".chat_design .main_content").removeClass("chat_cream");
        $(".chat_design .main_content").removeClass("chat_chocolate");
        $(".chat_design .main_content").removeClass("chat_silver");
        $(".chat_design .main_content").removeClass("chat_cobalt");
    });
    // for blue
    $(".theme_color #blue").click(function () {
        $(".chat_design .main_content").addClass("chat_blue");
        $(".chat_design .main_content").removeClass("chat_green");
        $(".chat_design .main_content").removeClass("chat_orange");
        $(".chat_design .main_content").removeClass("chat_red");
        $(".chat_design .main_content").removeClass("chat_yellow");
        $(".chat_design .main_content").removeClass("chat_lime");
        $(".chat_design .main_content").removeClass("chat_turquoise");
        $(".chat_design .main_content").removeClass("chat_pink");
        $(".chat_design .main_content").removeClass("chat_magenta");
        $(".chat_design .main_content").removeClass("chat_purple");
        $(".chat_design .main_content").removeClass("chat_cream");
        $(".chat_design .main_content").removeClass("chat_chocolate");
        $(".chat_design .main_content").removeClass("chat_silver");
        $(".chat_design .main_content").removeClass("chat_cobalt");
    });
    // for orange
    $(".theme_color #orange").click(function () {
        $(".chat_design .main_content").addClass("chat_orange");
        $(".chat_design .main_content").removeClass("chat_green");
        $(".chat_design .main_content").removeClass("chat_blue");
        $(".chat_design .main_content").removeClass("chat_red");
        $(".chat_design .main_content").removeClass("chat_yellow");
        $(".chat_design .main_content").removeClass("chat_lime");
        $(".chat_design .main_content").removeClass("chat_turquoise");
        $(".chat_design .main_content").removeClass("chat_pink");
        $(".chat_design .main_content").removeClass("chat_magenta");
        $(".chat_design .main_content").removeClass("chat_purple");
        $(".chat_design .main_content").removeClass("chat_cream");
        $(".chat_design .main_content").removeClass("chat_chocolate");
        $(".chat_design .main_content").removeClass("chat_silver");
        $(".chat_design .main_content").removeClass("chat_cobalt");
    });
    // for red
    $(".theme_color #red").click(function () {
        $(".chat_design .main_content").addClass("chat_red");
        $(".chat_design .main_content").removeClass("chat_green");
        $(".chat_design .main_content").removeClass("chat_blue");
        $(".chat_design .main_content").removeClass("chat_orange");
        $(".chat_design .main_content").removeClass("chat_yellow");
        $(".chat_design .main_content").removeClass("chat_lime");
        $(".chat_design .main_content").removeClass("chat_turquoise");
        $(".chat_design .main_content").removeClass("chat_pink");
        $(".chat_design .main_content").removeClass("chat_magenta");
        $(".chat_design .main_content").removeClass("chat_purple");
        $(".chat_design .main_content").removeClass("chat_cream");
        $(".chat_design .main_content").removeClass("chat_chocolate");
        $(".chat_design .main_content").removeClass("chat_silver");
        $(".chat_design .main_content").removeClass("chat_cobalt");
    });
    // for yellow
    $(".theme_color #yellow").click(function () {
        $(".chat_design .main_content").addClass("chat_yellow");
        $(".chat_design .main_content").removeClass("chat_green");
        $(".chat_design .main_content").removeClass("chat_blue");
        $(".chat_design .main_content").removeClass("chat_orange");
        $(".chat_design .main_content").removeClass("chat_red");
        $(".chat_design .main_content").removeClass("chat_lime");
        $(".chat_design .main_content").removeClass("chat_turquoise");
        $(".chat_design .main_content").removeClass("chat_pink");
        $(".chat_design .main_content").removeClass("chat_magenta");
        $(".chat_design .main_content").removeClass("chat_purple");
        $(".chat_design .main_content").removeClass("chat_cream");
        $(".chat_design .main_content").removeClass("chat_chocolate");
        $(".chat_design .main_content").removeClass("chat_silver");
        $(".chat_design .main_content").removeClass("chat_cobalt");
    });
    // for lime
    $(".theme_color #lime").click(function () {
        $(".chat_design .main_content").addClass("chat_lime");
        $(".chat_design .main_content").removeClass("chat_green");
        $(".chat_design .main_content").removeClass("chat_blue");
        $(".chat_design .main_content").removeClass("chat_orange");
        $(".chat_design .main_content").removeClass("chat_red");
        $(".chat_design .main_content").removeClass("chat_yellow");
        $(".chat_design .main_content").removeClass("chat_turquoise");
        $(".chat_design .main_content").removeClass("chat_pink");
        $(".chat_design .main_content").removeClass("chat_magenta");
        $(".chat_design .main_content").removeClass("chat_purple");
        $(".chat_design .main_content").removeClass("chat_cream");
        $(".chat_design .main_content").removeClass("chat_chocolate");
        $(".chat_design .main_content").removeClass("chat_silver");
        $(".chat_design .main_content").removeClass("chat_cobalt");
    });
    // for turquoise
    $(".theme_color #turquoise").click(function () {
        $(".chat_design .main_content").addClass("chat_turquoise");
        $(".chat_design .main_content").removeClass("chat_green");
        $(".chat_design .main_content").removeClass("chat_blue");
        $(".chat_design .main_content").removeClass("chat_orange");
        $(".chat_design .main_content").removeClass("chat_red");
        $(".chat_design .main_content").removeClass("chat_yellow");
        $(".chat_design .main_content").removeClass("chat_lime");
        $(".chat_design .main_content").removeClass("chat_pink");
        $(".chat_design .main_content").removeClass("chat_magenta");
        $(".chat_design .main_content").removeClass("chat_purple");
        $(".chat_design .main_content").removeClass("chat_cream");
        $(".chat_design .main_content").removeClass("chat_chocolate");
        $(".chat_design .main_content").removeClass("chat_silver");
        $(".chat_design .main_content").removeClass("chat_cobalt");
    });
    // for pink
    $(".theme_color #pink").click(function () {
        $(".chat_design .main_content").addClass("chat_pink");
        $(".chat_design .main_content").removeClass("chat_green");
        $(".chat_design .main_content").removeClass("chat_blue");
        $(".chat_design .main_content").removeClass("chat_orange");
        $(".chat_design .main_content").removeClass("chat_red");
        $(".chat_design .main_content").removeClass("chat_yellow");
        $(".chat_design .main_content").removeClass("chat_lime");
        $(".chat_design .main_content").removeClass("chat_turquoise");
        $(".chat_design .main_content").removeClass("chat_magenta");
        $(".chat_design .main_content").removeClass("chat_purple");
        $(".chat_design .main_content").removeClass("chat_cream");
        $(".chat_design .main_content").removeClass("chat_chocolate");
        $(".chat_design .main_content").removeClass("chat_silver");
        $(".chat_design .main_content").removeClass("chat_cobalt");
    });
    // for magenta
    $(".theme_color #magenta").click(function () {
        $(".chat_design .main_content").addClass("chat_magenta");
        $(".chat_design .main_content").removeClass("chat_green");
        $(".chat_design .main_content").removeClass("chat_blue");
        $(".chat_design .main_content").removeClass("chat_orange");
        $(".chat_design .main_content").removeClass("chat_red");
        $(".chat_design .main_content").removeClass("chat_yellow");
        $(".chat_design .main_content").removeClass("chat_lime");
        $(".chat_design .main_content").removeClass("chat_turquoise");
        $(".chat_design .main_content").removeClass("chat_pink");
        $(".chat_design .main_content").removeClass("chat_purple");
        $(".chat_design .main_content").removeClass("chat_cream");
        $(".chat_design .main_content").removeClass("chat_chocolate");
        $(".chat_design .main_content").removeClass("chat_silver");
        $(".chat_design .main_content").removeClass("chat_cobalt");
    });
    // for purple
    $(".theme_color #purple").click(function () {
        $(".chat_design .main_content").addClass("chat_purple");
        $(".chat_design .main_content").removeClass("chat_green");
        $(".chat_design .main_content").removeClass("chat_blue");
        $(".chat_design .main_content").removeClass("chat_orange");
        $(".chat_design .main_content").removeClass("chat_red");
        $(".chat_design .main_content").removeClass("chat_yellow");
        $(".chat_design .main_content").removeClass("chat_lime");
        $(".chat_design .main_content").removeClass("chat_turquoise");
        $(".chat_design .main_content").removeClass("chat_pink");
        $(".chat_design .main_content").removeClass("chat_magenta");
        $(".chat_design .main_content").removeClass("chat_cream");
        $(".chat_design .main_content").removeClass("chat_chocolate");
        $(".chat_design .main_content").removeClass("chat_silver");
        $(".chat_design .main_content").removeClass("chat_cobalt");
    });
    // for cream
    $(".theme_color #cream").click(function () {
        $(".chat_design .main_content").addClass("chat_cream");
        $(".chat_design .main_content").removeClass("chat_green");
        $(".chat_design .main_content").removeClass("chat_blue");
        $(".chat_design .main_content").removeClass("chat_orange");
        $(".chat_design .main_content").removeClass("chat_red");
        $(".chat_design .main_content").removeClass("chat_yellow");
        $(".chat_design .main_content").removeClass("chat_lime");
        $(".chat_design .main_content").removeClass("chat_turquoise");
        $(".chat_design .main_content").removeClass("chat_pink");
        $(".chat_design .main_content").removeClass("chat_magenta");
        $(".chat_design .main_content").removeClass("chat_purple");
        $(".chat_design .main_content").removeClass("chat_chocolate");
        $(".chat_design .main_content").removeClass("chat_silver");
        $(".chat_design .main_content").removeClass("chat_cobalt");
    });
    // for chocolate
    $(".theme_color #chocolate").click(function () {
        $(".chat_design .main_content").addClass("chat_chocolate");
        $(".chat_design .main_content").removeClass("chat_green");
        $(".chat_design .main_content").removeClass("chat_blue");
        $(".chat_design .main_content").removeClass("chat_orange");
        $(".chat_design .main_content").removeClass("chat_red");
        $(".chat_design .main_content").removeClass("chat_yellow");
        $(".chat_design .main_content").removeClass("chat_lime");
        $(".chat_design .main_content").removeClass("chat_turquoise");
        $(".chat_design .main_content").removeClass("chat_pink");
        $(".chat_design .main_content").removeClass("chat_magenta");
        $(".chat_design .main_content").removeClass("chat_purple");
        $(".chat_design .main_content").removeClass("chat_cream");
        $(".chat_design .main_content").removeClass("chat_silver");
        $(".chat_design .main_content").removeClass("chat_cobalt");
    });
    // for silver
    $(".theme_color #silver").click(function () {
        $(".chat_design .main_content").addClass("chat_silver");
        $(".chat_design .main_content").removeClass("chat_green");
        $(".chat_design .main_content").removeClass("chat_blue");
        $(".chat_design .main_content").removeClass("chat_orange");
        $(".chat_design .main_content").removeClass("chat_red");
        $(".chat_design .main_content").removeClass("chat_yellow");
        $(".chat_design .main_content").removeClass("chat_lime");
        $(".chat_design .main_content").removeClass("chat_turquoise");
        $(".chat_design .main_content").removeClass("chat_pink");
        $(".chat_design .main_content").removeClass("chat_magenta");
        $(".chat_design .main_content").removeClass("chat_purple");
        $(".chat_design .main_content").removeClass("chat_cream");
        $(".chat_design .main_content").removeClass("chat_chocolate");
        $(".chat_design .main_content").removeClass("chat_cobalt");
    });
    // for cobalt
    $(".theme_color #cobalt").click(function () {
        $(".chat_design .main_content").addClass("chat_cobalt");
        $(".chat_design .main_content").removeClass("chat_green");
        $(".chat_design .main_content").removeClass("chat_blue");
        $(".chat_design .main_content").removeClass("chat_orange");
        $(".chat_design .main_content").removeClass("chat_red");
        $(".chat_design .main_content").removeClass("chat_yellow");
        $(".chat_design .main_content").removeClass("chat_lime");
        $(".chat_design .main_content").removeClass("chat_turquoise");
        $(".chat_design .main_content").removeClass("chat_pink");
        $(".chat_design .main_content").removeClass("chat_magenta");
        $(".chat_design .main_content").removeClass("chat_purple");
        $(".chat_design .main_content").removeClass("chat_cream");
        $(".chat_design .main_content").removeClass("chat_chocolate");
        $(".chat_design .main_content").removeClass("chat_silver");
    });
});
// header color end 

