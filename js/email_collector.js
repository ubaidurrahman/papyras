// email_collector style changing trigger
$(".content_list li a").click(function () {
    if (this.id == "email_collector_trigger") {
        $(".email_collector").addClass("ec_design");
    } else {
        $(".email_collector").removeClass("ec_design");
    }
});

// theme changing start 
$(document).ready(function () {
    // for theme 01
    $(".theme_trigger #theme_01").click(function () {
        $(".ec_design").empty();
        $(".ec_design").append("<div class='content theme_01 e_text_light'><div class='background padding'><h2>For all things about content advertising</h2><h4>Join Papyras newsletter</h4><form action=''><div class='form-group'><input type='email' name='' placeholder='Email Address'></div><button type='submit'>Submit</button></form></div></div >");
    });
    // for theme 02
    $(".theme_trigger #theme_02").click(function () {
        $(".ec_design").empty();
        $(".ec_design").append("<div class='content theme_02 e_green'><div class= 'padding'><div class='row'><div class='col-sm-6'><div class='image'><img src='images/email_collector/1.png' alt=''></div></div><div class='col-sm-6'><h3>Join the community by subscribing</h3><h6>We try to avoid spam and deliver things that matter.</h6><p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using.</p><p>Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p><form action=''><div class='form-group'><input type='email' name='' placeholder='Your Email Address'></div><button type='submit'>Submit</button></form></div></div></div></div>");
    });
    // for theme 03
    $(".theme_trigger #theme_03").click(function () {
        $(".ec_design").empty();
        $(".ec_design").append("<div class='content theme_03 e_green'><div class= 'padding'><div class='row'><div class='col-sm-4'><div class='image'><img src='images/email_collector/2.png' alt=''></div></div><div class='col-sm-8'><h3>Join the community by subscribing</h3><p>It is a established will be distracted by the readable content of a page its layout. The point of using.</p><p class='pb-3'>Lorem Ipsum is that it has a distribution of letters.</p><form action=''><div class='row'><div class='col-sm-7'><div class='form-group'><input type='email' name='' placeholder='Your Email Address'></div></div><div class='col-sm-4 offset-sm-1'><button type='submit'>Submit</button></div></div></form></div></div></div></div>");
    });
    // for theme 04
    $(".theme_trigger #theme_04").click(function () {
        $(".ec_design").empty();
        $(".ec_design").append("<div class='content theme_04 e_green'><div class= 'row'><div class='col-sm-6'><div class='image_area'><div class='image'><img src='images/email_collector/1.png' alt=''></div></div></div><div class='col-sm-6'><div class='padding'><h3>Join the community by subscribing</h3><h6>We try to avoid spam and deliver things that matter.</h6><p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using.</p><p>Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p><form action=''><div class='form-group'><input type='email' name='' placeholder='Your Email Address'></div><button type='submit'>Submit</button></form></div></div></div></div>");
    });
    // for theme 05
    $(".theme_trigger #theme_05").click(function () {
        $(".ec_design").empty();
        $(".ec_design").append("<div class='content theme_05 e_green'><div class= 'padding'><div class='row'><div class='col-sm-6'><div class='image'><img src='images/email_collector/1.png' alt=''></div></div><div class='col-sm-6'><h3>Join the community by subscribing</h3><h6>We try to avoid spam and deliver things that matter.</h6><p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using.</p><p>Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p><form action=''><div class='form-group'><input type='email' name='' placeholder='Your Email Address'></div><button type='submit'>Submit</button></form></div></div></div></div>");
    });
    // for theme 06
    $(".theme_trigger #theme_06").click(function () {
        $(".ec_design").empty();
        $(".ec_design").append("<div class='content theme_06 e_green'><div class= 'padding'><div class='row'><div class='col-sm-4'><div class='image'><img src='images/email_collector/2.png' alt=''></div></div><div class='col-sm-8'><h3>Join the community by subscribing</h3><p>It is a established will be distracted by the readable content of a page its layout. The point of using.</p><p class='pb-3'>Lorem Ipsum is that it has a distribution of letters.</p><form action=''><div class='row'><div class='col-sm-7'><div class='form-group'><input type='email' name='' placeholder='Your Email Address'></div></div><div class='col-sm-4 offset-sm-1'><button type='submit'>Submit</button></div></div></form></div></div></div></div>");
    });
});
// theme changing end


// email_collector color start
$(document).ready(function () {
    // for green
    $(".theme_color #green").click(function () {
        $(".ec_design .content").addClass("e_green");
        $(".ec_design .content").removeClass("e_blue");
        $(".ec_design .content").removeClass("e_orange");
        $(".ec_design .content").removeClass("e_red");
        $(".ec_design .content").removeClass("e_yellow");
        $(".ec_design .content").removeClass("e_lime");
        $(".ec_design .content").removeClass("e_turquoise");
        $(".ec_design .content").removeClass("e_pink");
        $(".ec_design .content").removeClass("e_magenta");
        $(".ec_design .content").removeClass("e_purple");
        $(".ec_design .content").removeClass("e_cream");
        $(".ec_design .content").removeClass("e_chocolate");
        $(".ec_design .content").removeClass("e_silver");
        $(".ec_design .content").removeClass("e_cobalt");
    });
    // for blue
    $(".theme_color #blue").click(function () {
        $(".ec_design .content").addClass("e_blue");
        $(".ec_design .content").removeClass("e_green");
        $(".ec_design .content").removeClass("e_orange");
        $(".ec_design .content").removeClass("e_red");
        $(".ec_design .content").removeClass("e_yellow");
        $(".ec_design .content").removeClass("e_lime");
        $(".ec_design .content").removeClass("e_turquoise");
        $(".ec_design .content").removeClass("e_pink");
        $(".ec_design .content").removeClass("e_magenta");
        $(".ec_design .content").removeClass("e_purple");
        $(".ec_design .content").removeClass("e_cream");
        $(".ec_design .content").removeClass("e_chocolate");
        $(".ec_design .content").removeClass("e_silver");
        $(".ec_design .content").removeClass("e_cobalt");
    });
    // for orange
    $(".theme_color #orange").click(function () {
        $(".ec_design .content").addClass("e_orange");
        $(".ec_design .content").removeClass("e_green");
        $(".ec_design .content").removeClass("e_blue");
        $(".ec_design .content").removeClass("e_red");
        $(".ec_design .content").removeClass("e_yellow");
        $(".ec_design .content").removeClass("e_lime");
        $(".ec_design .content").removeClass("e_turquoise");
        $(".ec_design .content").removeClass("e_pink");
        $(".ec_design .content").removeClass("e_magenta");
        $(".ec_design .content").removeClass("e_purple");
        $(".ec_design .content").removeClass("e_cream");
        $(".ec_design .content").removeClass("e_chocolate");
        $(".ec_design .content").removeClass("e_silver");
        $(".ec_design .content").removeClass("e_cobalt");
    });
    // for red
    $(".theme_color #red").click(function () {
        $(".ec_design .content").addClass("e_red");
        $(".ec_design .content").removeClass("e_green");
        $(".ec_design .content").removeClass("e_blue");
        $(".ec_design .content").removeClass("e_orange");
        $(".ec_design .content").removeClass("e_yellow");
        $(".ec_design .content").removeClass("e_lime");
        $(".ec_design .content").removeClass("e_turquoise");
        $(".ec_design .content").removeClass("e_pink");
        $(".ec_design .content").removeClass("e_magenta");
        $(".ec_design .content").removeClass("e_purple");
        $(".ec_design .content").removeClass("e_cream");
        $(".ec_design .content").removeClass("e_chocolate");
        $(".ec_design .content").removeClass("e_silver");
        $(".ec_design .content").removeClass("e_cobalt");
    });
    // for yellow
    $(".theme_color #yellow").click(function () {
        $(".ec_design .content").addClass("e_yellow");
        $(".ec_design .content").removeClass("e_green");
        $(".ec_design .content").removeClass("e_blue");
        $(".ec_design .content").removeClass("e_orange");
        $(".ec_design .content").removeClass("e_red");
        $(".ec_design .content").removeClass("e_lime");
        $(".ec_design .content").removeClass("e_turquoise");
        $(".ec_design .content").removeClass("e_pink");
        $(".ec_design .content").removeClass("e_magenta");
        $(".ec_design .content").removeClass("e_purple");
        $(".ec_design .content").removeClass("e_cream");
        $(".ec_design .content").removeClass("e_chocolate");
        $(".ec_design .content").removeClass("e_silver");
        $(".ec_design .content").removeClass("e_cobalt");
    });
    // for lime
    $(".theme_color #lime").click(function () {
        $(".ec_design .content").addClass("e_lime");
        $(".ec_design .content").removeClass("e_green");
        $(".ec_design .content").removeClass("e_blue");
        $(".ec_design .content").removeClass("e_orange");
        $(".ec_design .content").removeClass("e_red");
        $(".ec_design .content").removeClass("e_yellow");
        $(".ec_design .content").removeClass("e_turquoise");
        $(".ec_design .content").removeClass("e_pink");
        $(".ec_design .content").removeClass("e_magenta");
        $(".ec_design .content").removeClass("e_purple");
        $(".ec_design .content").removeClass("e_cream");
        $(".ec_design .content").removeClass("e_chocolate");
        $(".ec_design .content").removeClass("e_silver");
        $(".ec_design .content").removeClass("e_cobalt");
    });
    // for turquoise
    $(".theme_color #turquoise").click(function () {
        $(".ec_design .content").addClass("e_turquoise");
        $(".ec_design .content").removeClass("e_green");
        $(".ec_design .content").removeClass("e_blue");
        $(".ec_design .content").removeClass("e_orange");
        $(".ec_design .content").removeClass("e_red");
        $(".ec_design .content").removeClass("e_yellow");
        $(".ec_design .content").removeClass("e_lime");
        $(".ec_design .content").removeClass("e_pink");
        $(".ec_design .content").removeClass("e_magenta");
        $(".ec_design .content").removeClass("e_purple");
        $(".ec_design .content").removeClass("e_cream");
        $(".ec_design .content").removeClass("e_chocolate");
        $(".ec_design .content").removeClass("e_silver");
        $(".ec_design .content").removeClass("e_cobalt");
    });
    // for pink
    $(".theme_color #pink").click(function () {
        $(".ec_design .content").addClass("e_pink");
        $(".ec_design .content").removeClass("e_green");
        $(".ec_design .content").removeClass("e_blue");
        $(".ec_design .content").removeClass("e_orange");
        $(".ec_design .content").removeClass("e_red");
        $(".ec_design .content").removeClass("e_yellow");
        $(".ec_design .content").removeClass("e_lime");
        $(".ec_design .content").removeClass("e_turquoise");
        $(".ec_design .content").removeClass("e_magenta");
        $(".ec_design .content").removeClass("e_purple");
        $(".ec_design .content").removeClass("e_cream");
        $(".ec_design .content").removeClass("e_chocolate");
        $(".ec_design .content").removeClass("e_silver");
        $(".ec_design .content").removeClass("e_cobalt");
    });
    // for magenta
    $(".theme_color #magenta").click(function () {
        $(".ec_design .content").addClass("e_magenta");
        $(".ec_design .content").removeClass("e_green");
        $(".ec_design .content").removeClass("e_blue");
        $(".ec_design .content").removeClass("e_orange");
        $(".ec_design .content").removeClass("e_red");
        $(".ec_design .content").removeClass("e_yellow");
        $(".ec_design .content").removeClass("e_lime");
        $(".ec_design .content").removeClass("e_turquoise");
        $(".ec_design .content").removeClass("e_pink");
        $(".ec_design .content").removeClass("e_purple");
        $(".ec_design .content").removeClass("e_cream");
        $(".ec_design .content").removeClass("e_chocolate");
        $(".ec_design .content").removeClass("e_silver");
        $(".ec_design .content").removeClass("e_cobalt");
    });
    // for purple
    $(".theme_color #purple").click(function () {
        $(".ec_design .content").addClass("e_purple");
        $(".ec_design .content").removeClass("e_green");
        $(".ec_design .content").removeClass("e_blue");
        $(".ec_design .content").removeClass("e_orange");
        $(".ec_design .content").removeClass("e_red");
        $(".ec_design .content").removeClass("e_yellow");
        $(".ec_design .content").removeClass("e_lime");
        $(".ec_design .content").removeClass("e_turquoise");
        $(".ec_design .content").removeClass("e_pink");
        $(".ec_design .content").removeClass("e_magenta");
        $(".ec_design .content").removeClass("e_cream");
        $(".ec_design .content").removeClass("e_chocolate");
        $(".ec_design .content").removeClass("e_silver");
        $(".ec_design .content").removeClass("e_cobalt");
    });
    // for cream
    $(".theme_color #cream").click(function () {
        $(".ec_design .content").addClass("e_cream");
        $(".ec_design .content").removeClass("e_green");
        $(".ec_design .content").removeClass("e_blue");
        $(".ec_design .content").removeClass("e_orange");
        $(".ec_design .content").removeClass("e_red");
        $(".ec_design .content").removeClass("e_yellow");
        $(".ec_design .content").removeClass("e_lime");
        $(".ec_design .content").removeClass("e_turquoise");
        $(".ec_design .content").removeClass("e_pink");
        $(".ec_design .content").removeClass("e_magenta");
        $(".ec_design .content").removeClass("e_purple");
        $(".ec_design .content").removeClass("e_chocolate");
        $(".ec_design .content").removeClass("e_silver");
        $(".ec_design .content").removeClass("e_cobalt");
    });
    // for chocolate
    $(".theme_color #chocolate").click(function () {
        $(".ec_design .content").addClass("e_chocolate");
        $(".ec_design .content").removeClass("e_green");
        $(".ec_design .content").removeClass("e_blue");
        $(".ec_design .content").removeClass("e_orange");
        $(".ec_design .content").removeClass("e_red");
        $(".ec_design .content").removeClass("e_yellow");
        $(".ec_design .content").removeClass("e_lime");
        $(".ec_design .content").removeClass("e_turquoise");
        $(".ec_design .content").removeClass("e_pink");
        $(".ec_design .content").removeClass("e_magenta");
        $(".ec_design .content").removeClass("e_purple");
        $(".ec_design .content").removeClass("e_cream");
        $(".ec_design .content").removeClass("e_silver");
        $(".ec_design .content").removeClass("e_cobalt");
    });
    // for silver
    $(".theme_color #silver").click(function () {
        $(".ec_design .content").addClass("e_silver");
        $(".ec_design .content").removeClass("e_green");
        $(".ec_design .content").removeClass("e_blue");
        $(".ec_design .content").removeClass("e_orange");
        $(".ec_design .content").removeClass("e_red");
        $(".ec_design .content").removeClass("e_yellow");
        $(".ec_design .content").removeClass("e_lime");
        $(".ec_design .content").removeClass("e_turquoise");
        $(".ec_design .content").removeClass("e_pink");
        $(".ec_design .content").removeClass("e_magenta");
        $(".ec_design .content").removeClass("e_purple");
        $(".ec_design .content").removeClass("e_cream");
        $(".ec_design .content").removeClass("e_chocolate");
        $(".ec_design .content").removeClass("e_cobalt");
    });
    // for cobalt
    $(".theme_color #cobalt").click(function () {
        $(".ec_design .content").addClass("e_cobalt");
        $(".ec_design .content").removeClass("e_green");
        $(".ec_design .content").removeClass("e_blue");
        $(".ec_design .content").removeClass("e_orange");
        $(".ec_design .content").removeClass("e_red");
        $(".ec_design .content").removeClass("e_yellow");
        $(".ec_design .content").removeClass("e_lime");
        $(".ec_design .content").removeClass("e_turquoise");
        $(".ec_design .content").removeClass("e_pink");
        $(".ec_design .content").removeClass("e_magenta");
        $(".ec_design .content").removeClass("e_purple");
        $(".ec_design .content").removeClass("e_cream");
        $(".ec_design .content").removeClass("e_chocolate");
        $(".ec_design .content").removeClass("e_silver");
    });
});
// email_collector color end 


// text color start 
$(document).ready(function () {
    // for text light 
    $(".text_color #text_light").click(function () {
        $(".ec_design .content").addClass("e_text_light");
        $(".ec_design .content").removeClass("e_text_dark");
    });
    // for text dark 
    $(".text_color #text_dark").click(function () {
        $(".ec_design .content").addClass("e_text_dark");
        $(".ec_design .content").removeClass("e_text_light");
    });
});