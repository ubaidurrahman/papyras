// header style change
$(".content_list li a").click(function () {
    if (this.id == "header_blogger_trigger"){
        $(".design_view_area .header .nav_bottom").addClass("header_blogger");
    }else {
        $(".design_view_area .header .nav_bottom").removeClass("header_blogger");
    }
});



// header color start
$(document).ready(function () {
    // for green
    $(".theme_color #green").click(function () {
        $(".header_blogger").addClass("h_green");
        $(".header_blogger").removeClass("h_blue");
        $(".header_blogger").removeClass("h_orange");
        $(".header_blogger").removeClass("h_red");
        $(".header_blogger").removeClass("h_yellow");
        $(".header_blogger").removeClass("h_lime");
        $(".header_blogger").removeClass("h_turquoise");
        $(".header_blogger").removeClass("h_pink");
        $(".header_blogger").removeClass("h_magenta");
        $(".header_blogger").removeClass("h_purple");
        $(".header_blogger").removeClass("h_cream");
        $(".header_blogger").removeClass("h_chocolate");
        $(".header_blogger").removeClass("h_silver");
        $(".header_blogger").removeClass("h_cobalt");
    });
    // for blue
    $(".theme_color #blue").click(function () {
        $(".header_blogger").addClass("h_blue");
        $(".header_blogger").removeClass("h_green");
        $(".header_blogger").removeClass("h_orange");
        $(".header_blogger").removeClass("h_red");
        $(".header_blogger").removeClass("h_yellow");
        $(".header_blogger").removeClass("h_lime");
        $(".header_blogger").removeClass("h_turquoise");
        $(".header_blogger").removeClass("h_pink");
        $(".header_blogger").removeClass("h_magenta");
        $(".header_blogger").removeClass("h_purple");
        $(".header_blogger").removeClass("h_cream");
        $(".header_blogger").removeClass("h_chocolate");
        $(".header_blogger").removeClass("h_silver");
        $(".header_blogger").removeClass("h_cobalt");
    });
    // for orange
    $(".theme_color #orange").click(function () {
        $(".header_blogger").addClass("h_orange");
        $(".header_blogger").removeClass("h_green");
        $(".header_blogger").removeClass("h_blue");
        $(".header_blogger").removeClass("h_red");
        $(".header_blogger").removeClass("h_yellow");
        $(".header_blogger").removeClass("h_lime");
        $(".header_blogger").removeClass("h_turquoise");
        $(".header_blogger").removeClass("h_pink");
        $(".header_blogger").removeClass("h_magenta");
        $(".header_blogger").removeClass("h_purple");
        $(".header_blogger").removeClass("h_cream");
        $(".header_blogger").removeClass("h_chocolate");
        $(".header_blogger").removeClass("h_silver");
        $(".header_blogger").removeClass("h_cobalt");
    });
    // for red
    $(".theme_color #red").click(function () {
        $(".header_blogger").addClass("h_red");
        $(".header_blogger").removeClass("h_green");
        $(".header_blogger").removeClass("h_blue");
        $(".header_blogger").removeClass("h_orange");
        $(".header_blogger").removeClass("h_yellow");
        $(".header_blogger").removeClass("h_lime");
        $(".header_blogger").removeClass("h_turquoise");
        $(".header_blogger").removeClass("h_pink");
        $(".header_blogger").removeClass("h_magenta");
        $(".header_blogger").removeClass("h_purple");
        $(".header_blogger").removeClass("h_cream");
        $(".header_blogger").removeClass("h_chocolate");
        $(".header_blogger").removeClass("h_silver");
        $(".header_blogger").removeClass("h_cobalt");
    });
    // for yellow
    $(".theme_color #yellow").click(function () {
        $(".header_blogger").addClass("h_yellow");
        $(".header_blogger").removeClass("h_green");
        $(".header_blogger").removeClass("h_blue");
        $(".header_blogger").removeClass("h_orange");
        $(".header_blogger").removeClass("h_red");
        $(".header_blogger").removeClass("h_lime");
        $(".header_blogger").removeClass("h_turquoise");
        $(".header_blogger").removeClass("h_pink");
        $(".header_blogger").removeClass("h_magenta");
        $(".header_blogger").removeClass("h_purple");
        $(".header_blogger").removeClass("h_cream");
        $(".header_blogger").removeClass("h_chocolate");
        $(".header_blogger").removeClass("h_silver");
        $(".header_blogger").removeClass("h_cobalt");
    });
    // for lime
    $(".theme_color #lime").click(function () {
        $(".header_blogger").addClass("h_lime");
        $(".header_blogger").removeClass("h_green");
        $(".header_blogger").removeClass("h_blue");
        $(".header_blogger").removeClass("h_orange");
        $(".header_blogger").removeClass("h_red");
        $(".header_blogger").removeClass("h_yellow");
        $(".header_blogger").removeClass("h_turquoise");
        $(".header_blogger").removeClass("h_pink");
        $(".header_blogger").removeClass("h_magenta");
        $(".header_blogger").removeClass("h_purple");
        $(".header_blogger").removeClass("h_cream");
        $(".header_blogger").removeClass("h_chocolate");
        $(".header_blogger").removeClass("h_silver");
        $(".header_blogger").removeClass("h_cobalt");
    });
    // for turquoise
    $(".theme_color #turquoise").click(function () {
        $(".header_blogger").addClass("h_turquoise");
        $(".header_blogger").removeClass("h_green");
        $(".header_blogger").removeClass("h_blue");
        $(".header_blogger").removeClass("h_orange");
        $(".header_blogger").removeClass("h_red");
        $(".header_blogger").removeClass("h_yellow");
        $(".header_blogger").removeClass("h_lime");
        $(".header_blogger").removeClass("h_pink");
        $(".header_blogger").removeClass("h_magenta");
        $(".header_blogger").removeClass("h_purple");
        $(".header_blogger").removeClass("h_cream");
        $(".header_blogger").removeClass("h_chocolate");
        $(".header_blogger").removeClass("h_silver");
        $(".header_blogger").removeClass("h_cobalt");
    });
    // for pink
    $(".theme_color #pink").click(function () {
        $(".header_blogger").addClass("h_pink");
        $(".header_blogger").removeClass("h_green");
        $(".header_blogger").removeClass("h_blue");
        $(".header_blogger").removeClass("h_orange");
        $(".header_blogger").removeClass("h_red");
        $(".header_blogger").removeClass("h_yellow");
        $(".header_blogger").removeClass("h_lime");
        $(".header_blogger").removeClass("h_turquoise");
        $(".header_blogger").removeClass("h_magenta");
        $(".header_blogger").removeClass("h_purple");
        $(".header_blogger").removeClass("h_cream");
        $(".header_blogger").removeClass("h_chocolate");
        $(".header_blogger").removeClass("h_silver");
        $(".header_blogger").removeClass("h_cobalt");
    });
    // for magenta
    $(".theme_color #magenta").click(function () {
        $(".header_blogger").addClass("h_magenta");
        $(".header_blogger").removeClass("h_green");
        $(".header_blogger").removeClass("h_blue");
        $(".header_blogger").removeClass("h_orange");
        $(".header_blogger").removeClass("h_red");
        $(".header_blogger").removeClass("h_yellow");
        $(".header_blogger").removeClass("h_lime");
        $(".header_blogger").removeClass("h_turquoise");
        $(".header_blogger").removeClass("h_pink");
        $(".header_blogger").removeClass("h_purple");
        $(".header_blogger").removeClass("h_cream");
        $(".header_blogger").removeClass("h_chocolate");
        $(".header_blogger").removeClass("h_silver");
        $(".header_blogger").removeClass("h_cobalt");
    });
    // for purple
    $(".theme_color #purple").click(function () {
        $(".header_blogger").addClass("h_purple");
        $(".header_blogger").removeClass("h_green");
        $(".header_blogger").removeClass("h_blue");
        $(".header_blogger").removeClass("h_orange");
        $(".header_blogger").removeClass("h_red");
        $(".header_blogger").removeClass("h_yellow");
        $(".header_blogger").removeClass("h_lime");
        $(".header_blogger").removeClass("h_turquoise");
        $(".header_blogger").removeClass("h_pink");
        $(".header_blogger").removeClass("h_magenta");
        $(".header_blogger").removeClass("h_cream");
        $(".header_blogger").removeClass("h_chocolate");
        $(".header_blogger").removeClass("h_silver");
        $(".header_blogger").removeClass("h_cobalt");
    });
    // for cream
    $(".theme_color #cream").click(function () {
        $(".header_blogger").addClass("h_cream");
        $(".header_blogger").removeClass("h_green");
        $(".header_blogger").removeClass("h_blue");
        $(".header_blogger").removeClass("h_orange");
        $(".header_blogger").removeClass("h_red");
        $(".header_blogger").removeClass("h_yellow");
        $(".header_blogger").removeClass("h_lime");
        $(".header_blogger").removeClass("h_turquoise");
        $(".header_blogger").removeClass("h_pink");
        $(".header_blogger").removeClass("h_magenta");
        $(".header_blogger").removeClass("h_purple");
        $(".header_blogger").removeClass("h_chocolate");
        $(".header_blogger").removeClass("h_silver");
        $(".header_blogger").removeClass("h_cobalt");
    });
    // for chocolate
    $(".theme_color #chocolate").click(function () {
        $(".header_blogger").addClass("h_chocolate");
        $(".header_blogger").removeClass("h_green");
        $(".header_blogger").removeClass("h_blue");
        $(".header_blogger").removeClass("h_orange");
        $(".header_blogger").removeClass("h_red");
        $(".header_blogger").removeClass("h_yellow");
        $(".header_blogger").removeClass("h_lime");
        $(".header_blogger").removeClass("h_turquoise");
        $(".header_blogger").removeClass("h_pink");
        $(".header_blogger").removeClass("h_magenta");
        $(".header_blogger").removeClass("h_purple");
        $(".header_blogger").removeClass("h_cream");
        $(".header_blogger").removeClass("h_silver");
        $(".header_blogger").removeClass("h_cobalt");
    });
    // for silver
    $(".theme_color #silver").click(function () {
        $(".header_blogger").addClass("h_silver");
        $(".header_blogger").removeClass("h_green");
        $(".header_blogger").removeClass("h_blue");
        $(".header_blogger").removeClass("h_orange");
        $(".header_blogger").removeClass("h_red");
        $(".header_blogger").removeClass("h_yellow");
        $(".header_blogger").removeClass("h_lime");
        $(".header_blogger").removeClass("h_turquoise");
        $(".header_blogger").removeClass("h_pink");
        $(".header_blogger").removeClass("h_magenta");
        $(".header_blogger").removeClass("h_purple");
        $(".header_blogger").removeClass("h_cream");
        $(".header_blogger").removeClass("h_chocolate");
        $(".header_blogger").removeClass("h_cobalt");
    });
    // for cobalt
    $(".theme_color #cobalt").click(function () {
        $(".header_blogger").addClass("h_cobalt");
        $(".header_blogger").removeClass("h_green");
        $(".header_blogger").removeClass("h_blue");
        $(".header_blogger").removeClass("h_orange");
        $(".header_blogger").removeClass("h_red");
        $(".header_blogger").removeClass("h_yellow");
        $(".header_blogger").removeClass("h_lime");
        $(".header_blogger").removeClass("h_turquoise");
        $(".header_blogger").removeClass("h_pink");
        $(".header_blogger").removeClass("h_magenta");
        $(".header_blogger").removeClass("h_purple");
        $(".header_blogger").removeClass("h_cream");
        $(".header_blogger").removeClass("h_chocolate");
        $(".header_blogger").removeClass("h_silver");
    });
});
// header color end 


// text color start 
$(document).ready(function () {
    // for text light 
    $(".text_color #text_light").click(function () {
        $(".header_blogger").addClass("h_text_light");
        $(".header_blogger").removeClass("h_text_dark");
    });
    // for text dark 
    $(".text_color #text_dark").click(function () {
        $(".header_blogger").addClass("h_text_dark");
        $(".header_blogger").removeClass("h_text_light");
    });
});