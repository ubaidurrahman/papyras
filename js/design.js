// for aside active color
$('.content_list li a').on('click', function () {
    $('.content_list li a.active').removeClass('active');
    $(this).addClass('active');
});


// for popup
$(document).ready(function () {
    $('.onload_modal').on('show.bs.modal', function () {
        var mod = $('.modal');
        $('.design_view_area_popup').html(mod);
    });
});



// tutorial content changing
// for header
$(".content_list #header_blogger_trigger").click(function () {
    $(".tutorial_area .content").empty();
    $(".tutorial_area .content").append("<p>Header is ready to start modifying.</p>");
});

// for Email Collector
$(".content_list #email_collector_trigger").click(function () {
    $(".tutorial_area .content").empty();
    $(".tutorial_area .content").append("<p>Email Collector is ready to start modifying.</p>");
});

// for onload pop-up
$(".content_list #popup_trigger").click(function () {
    $(".tutorial_area .content").empty();
    $(".tutorial_area .content").append("<p>Pop-up is ready to start modifying.</p>");
});

// for Feedback
$(".content_list #feedback_trigger").click(function () {
    $(".tutorial_area .content").empty();
    $(".tutorial_area .content").append("<p>Feedback is ready to start modifying.</p>");
});

// for chat
$(".content_list #chat_trigger").click(function () {
    $(".tutorial_area .content").empty();
    $(".tutorial_area .content").append("<p>Chat is ready to start modifying.</p>");
});

// for Bottom Feedback
$(".content_list #bottom_feedback_trigger").click(function () {
    $(".tutorial_area .content").empty();
    $(".tutorial_area .content").append("<p>Bottom Feedback is ready to start modifying.</p>");
});

// for Call to Action
$(".content_list #call_to_action_trigger").click(function () {
    $(".tutorial_area .content").empty();
    $(".tutorial_area .content").append("<p>Call to Action is ready to start modifying.</p>");
});

// for Quiz
$(".content_list #quiz_trigger").click(function () {
    $(".tutorial_area .content").empty();
    $(".tutorial_area .content").append("<p>Coming Soon...</p>");
});

// for Webinar Join
$(".content_list #webinar_join_trigger").click(function () {
    $(".tutorial_area .content").empty();
    $(".tutorial_area .content").append("<p>Coming Soon...</p>");
});

// for A/B Testing
$(".content_list #ab_testing_trigger").click(function () {
    $(".tutorial_area .content").empty();
    $(".tutorial_area .content").append("<p>Coming Soon...</p>");
});

// for Form
$(".content_list #form_trigger").click(function () {
    $(".tutorial_area .content").empty();
    $(".tutorial_area .content").append("<p>Coming Soon...</p>");
});

// for Poll
$(".content_list #poll_trigger").click(function () {
    $(".tutorial_area .content").empty();
    $(".tutorial_area .content").append("<p>Coming Soon...</p>");
});