// feedback style changing trigger
$(".content_list li a").click(function () {
    if (this.id == "bottom_feedback_trigger") {
        $(".feedback_bottom").addClass("fb_b_design");
        $(".feedback_bottom").addClass("feedback_bottom_show");
    } else {
        $(".feedback_bottom").removeClass("fb_b_design");
        $(".feedback_bottom").removeClass("feedback_bottom_show");
    }
});
$(document).ready(function () {
    $(".feedback_bottom_close").click(function () {
        $(".feedback_bottom").removeClass("feedback_bottom_show");
    });
});


// header color start
$(document).ready(function () {
    // for green
    $(".theme_color #green").click(function () {
        $(".fb_b_design .content").addClass("fb_b_green");
        $(".fb_b_design .content").removeClass("fb_b_blue");
        $(".fb_b_design .content").removeClass("fb_b_orange");
        $(".fb_b_design .content").removeClass("fb_b_red");
        $(".fb_b_design .content").removeClass("fb_b_yellow");
        $(".fb_b_design .content").removeClass("fb_b_lime");
        $(".fb_b_design .content").removeClass("fb_b_turquoise");
        $(".fb_b_design .content").removeClass("fb_b_pink");
        $(".fb_b_design .content").removeClass("fb_b_magenta");
        $(".fb_b_design .content").removeClass("fb_b_purple");
        $(".fb_b_design .content").removeClass("fb_b_cream");
        $(".fb_b_design .content").removeClass("fb_b_chocolate");
        $(".fb_b_design .content").removeClass("fb_b_silver");
        $(".fb_b_design .content").removeClass("fb_b_cobalt");
    });
    // for blue
    $(".theme_color #blue").click(function () {
        $(".fb_b_design .content").addClass("fb_b_blue");
        $(".fb_b_design .content").removeClass("fb_b_green");
        $(".fb_b_design .content").removeClass("fb_b_orange");
        $(".fb_b_design .content").removeClass("fb_b_red");
        $(".fb_b_design .content").removeClass("fb_b_yellow");
        $(".fb_b_design .content").removeClass("fb_b_lime");
        $(".fb_b_design .content").removeClass("fb_b_turquoise");
        $(".fb_b_design .content").removeClass("fb_b_pink");
        $(".fb_b_design .content").removeClass("fb_b_magenta");
        $(".fb_b_design .content").removeClass("fb_b_purple");
        $(".fb_b_design .content").removeClass("fb_b_cream");
        $(".fb_b_design .content").removeClass("fb_b_chocolate");
        $(".fb_b_design .content").removeClass("fb_b_silver");
        $(".fb_b_design .content").removeClass("fb_b_cobalt");
    });
    // for orange
    $(".theme_color #orange").click(function () {
        $(".fb_b_design .content").addClass("fb_b_orange");
        $(".fb_b_design .content").removeClass("fb_b_green");
        $(".fb_b_design .content").removeClass("fb_b_blue");
        $(".fb_b_design .content").removeClass("fb_b_red");
        $(".fb_b_design .content").removeClass("fb_b_yellow");
        $(".fb_b_design .content").removeClass("fb_b_lime");
        $(".fb_b_design .content").removeClass("fb_b_turquoise");
        $(".fb_b_design .content").removeClass("fb_b_pink");
        $(".fb_b_design .content").removeClass("fb_b_magenta");
        $(".fb_b_design .content").removeClass("fb_b_purple");
        $(".fb_b_design .content").removeClass("fb_b_cream");
        $(".fb_b_design .content").removeClass("fb_b_chocolate");
        $(".fb_b_design .content").removeClass("fb_b_silver");
        $(".fb_b_design .content").removeClass("fb_b_cobalt");
    });
    // for red
    $(".theme_color #red").click(function () {
        $(".fb_b_design .content").addClass("fb_b_red");
        $(".fb_b_design .content").removeClass("fb_b_green");
        $(".fb_b_design .content").removeClass("fb_b_blue");
        $(".fb_b_design .content").removeClass("fb_b_orange");
        $(".fb_b_design .content").removeClass("fb_b_yellow");
        $(".fb_b_design .content").removeClass("fb_b_lime");
        $(".fb_b_design .content").removeClass("fb_b_turquoise");
        $(".fb_b_design .content").removeClass("fb_b_pink");
        $(".fb_b_design .content").removeClass("fb_b_magenta");
        $(".fb_b_design .content").removeClass("fb_b_purple");
        $(".fb_b_design .content").removeClass("fb_b_cream");
        $(".fb_b_design .content").removeClass("fb_b_chocolate");
        $(".fb_b_design .content").removeClass("fb_b_silver");
        $(".fb_b_design .content").removeClass("fb_b_cobalt");
    });
    // for yellow
    $(".theme_color #yellow").click(function () {
        $(".fb_b_design .content").addClass("fb_b_yellow");
        $(".fb_b_design .content").removeClass("fb_b_green");
        $(".fb_b_design .content").removeClass("fb_b_blue");
        $(".fb_b_design .content").removeClass("fb_b_orange");
        $(".fb_b_design .content").removeClass("fb_b_red");
        $(".fb_b_design .content").removeClass("fb_b_lime");
        $(".fb_b_design .content").removeClass("fb_b_turquoise");
        $(".fb_b_design .content").removeClass("fb_b_pink");
        $(".fb_b_design .content").removeClass("fb_b_magenta");
        $(".fb_b_design .content").removeClass("fb_b_purple");
        $(".fb_b_design .content").removeClass("fb_b_cream");
        $(".fb_b_design .content").removeClass("fb_b_chocolate");
        $(".fb_b_design .content").removeClass("fb_b_silver");
        $(".fb_b_design .content").removeClass("fb_b_cobalt");
    });
    // for lime
    $(".theme_color #lime").click(function () {
        $(".fb_b_design .content").addClass("fb_b_lime");
        $(".fb_b_design .content").removeClass("fb_b_green");
        $(".fb_b_design .content").removeClass("fb_b_blue");
        $(".fb_b_design .content").removeClass("fb_b_orange");
        $(".fb_b_design .content").removeClass("fb_b_red");
        $(".fb_b_design .content").removeClass("fb_b_yellow");
        $(".fb_b_design .content").removeClass("fb_b_turquoise");
        $(".fb_b_design .content").removeClass("fb_b_pink");
        $(".fb_b_design .content").removeClass("fb_b_magenta");
        $(".fb_b_design .content").removeClass("fb_b_purple");
        $(".fb_b_design .content").removeClass("fb_b_cream");
        $(".fb_b_design .content").removeClass("fb_b_chocolate");
        $(".fb_b_design .content").removeClass("fb_b_silver");
        $(".fb_b_design .content").removeClass("fb_b_cobalt");
    });
    // for turquoise
    $(".theme_color #turquoise").click(function () {
        $(".fb_b_design .content").addClass("fb_b_turquoise");
        $(".fb_b_design .content").removeClass("fb_b_green");
        $(".fb_b_design .content").removeClass("fb_b_blue");
        $(".fb_b_design .content").removeClass("fb_b_orange");
        $(".fb_b_design .content").removeClass("fb_b_red");
        $(".fb_b_design .content").removeClass("fb_b_yellow");
        $(".fb_b_design .content").removeClass("fb_b_lime");
        $(".fb_b_design .content").removeClass("fb_b_pink");
        $(".fb_b_design .content").removeClass("fb_b_magenta");
        $(".fb_b_design .content").removeClass("fb_b_purple");
        $(".fb_b_design .content").removeClass("fb_b_cream");
        $(".fb_b_design .content").removeClass("fb_b_chocolate");
        $(".fb_b_design .content").removeClass("fb_b_silver");
        $(".fb_b_design .content").removeClass("fb_b_cobalt");
    });
    // for pink
    $(".theme_color #pink").click(function () {
        $(".fb_b_design .content").addClass("fb_b_pink");
        $(".fb_b_design .content").removeClass("fb_b_green");
        $(".fb_b_design .content").removeClass("fb_b_blue");
        $(".fb_b_design .content").removeClass("fb_b_orange");
        $(".fb_b_design .content").removeClass("fb_b_red");
        $(".fb_b_design .content").removeClass("fb_b_yellow");
        $(".fb_b_design .content").removeClass("fb_b_lime");
        $(".fb_b_design .content").removeClass("fb_b_turquoise");
        $(".fb_b_design .content").removeClass("fb_b_magenta");
        $(".fb_b_design .content").removeClass("fb_b_purple");
        $(".fb_b_design .content").removeClass("fb_b_cream");
        $(".fb_b_design .content").removeClass("fb_b_chocolate");
        $(".fb_b_design .content").removeClass("fb_b_silver");
        $(".fb_b_design .content").removeClass("fb_b_cobalt");
    });
    // for magenta
    $(".theme_color #magenta").click(function () {
        $(".fb_b_design .content").addClass("fb_b_magenta");
        $(".fb_b_design .content").removeClass("fb_b_green");
        $(".fb_b_design .content").removeClass("fb_b_blue");
        $(".fb_b_design .content").removeClass("fb_b_orange");
        $(".fb_b_design .content").removeClass("fb_b_red");
        $(".fb_b_design .content").removeClass("fb_b_yellow");
        $(".fb_b_design .content").removeClass("fb_b_lime");
        $(".fb_b_design .content").removeClass("fb_b_turquoise");
        $(".fb_b_design .content").removeClass("fb_b_pink");
        $(".fb_b_design .content").removeClass("fb_b_purple");
        $(".fb_b_design .content").removeClass("fb_b_cream");
        $(".fb_b_design .content").removeClass("fb_b_chocolate");
        $(".fb_b_design .content").removeClass("fb_b_silver");
        $(".fb_b_design .content").removeClass("fb_b_cobalt");
    });
    // for purple
    $(".theme_color #purple").click(function () {
        $(".fb_b_design .content").addClass("fb_b_purple");
        $(".fb_b_design .content").removeClass("fb_b_green");
        $(".fb_b_design .content").removeClass("fb_b_blue");
        $(".fb_b_design .content").removeClass("fb_b_orange");
        $(".fb_b_design .content").removeClass("fb_b_red");
        $(".fb_b_design .content").removeClass("fb_b_yellow");
        $(".fb_b_design .content").removeClass("fb_b_lime");
        $(".fb_b_design .content").removeClass("fb_b_turquoise");
        $(".fb_b_design .content").removeClass("fb_b_pink");
        $(".fb_b_design .content").removeClass("fb_b_magenta");
        $(".fb_b_design .content").removeClass("fb_b_cream");
        $(".fb_b_design .content").removeClass("fb_b_chocolate");
        $(".fb_b_design .content").removeClass("fb_b_silver");
        $(".fb_b_design .content").removeClass("fb_b_cobalt");
    });
    // for cream
    $(".theme_color #cream").click(function () {
        $(".fb_b_design .content").addClass("fb_b_cream");
        $(".fb_b_design .content").removeClass("fb_b_green");
        $(".fb_b_design .content").removeClass("fb_b_blue");
        $(".fb_b_design .content").removeClass("fb_b_orange");
        $(".fb_b_design .content").removeClass("fb_b_red");
        $(".fb_b_design .content").removeClass("fb_b_yellow");
        $(".fb_b_design .content").removeClass("fb_b_lime");
        $(".fb_b_design .content").removeClass("fb_b_turquoise");
        $(".fb_b_design .content").removeClass("fb_b_pink");
        $(".fb_b_design .content").removeClass("fb_b_magenta");
        $(".fb_b_design .content").removeClass("fb_b_purple");
        $(".fb_b_design .content").removeClass("fb_b_chocolate");
        $(".fb_b_design .content").removeClass("fb_b_silver");
        $(".fb_b_design .content").removeClass("fb_b_cobalt");
    });
    // for chocolate
    $(".theme_color #chocolate").click(function () {
        $(".fb_b_design .content").addClass("fb_b_chocolate");
        $(".fb_b_design .content").removeClass("fb_b_green");
        $(".fb_b_design .content").removeClass("fb_b_blue");
        $(".fb_b_design .content").removeClass("fb_b_orange");
        $(".fb_b_design .content").removeClass("fb_b_red");
        $(".fb_b_design .content").removeClass("fb_b_yellow");
        $(".fb_b_design .content").removeClass("fb_b_lime");
        $(".fb_b_design .content").removeClass("fb_b_turquoise");
        $(".fb_b_design .content").removeClass("fb_b_pink");
        $(".fb_b_design .content").removeClass("fb_b_magenta");
        $(".fb_b_design .content").removeClass("fb_b_purple");
        $(".fb_b_design .content").removeClass("fb_b_cream");
        $(".fb_b_design .content").removeClass("fb_b_silver");
        $(".fb_b_design .content").removeClass("fb_b_cobalt");
    });
    // for silver
    $(".theme_color #silver").click(function () {
        $(".fb_b_design .content").addClass("fb_b_silver");
        $(".fb_b_design .content").removeClass("fb_b_green");
        $(".fb_b_design .content").removeClass("fb_b_blue");
        $(".fb_b_design .content").removeClass("fb_b_orange");
        $(".fb_b_design .content").removeClass("fb_b_red");
        $(".fb_b_design .content").removeClass("fb_b_yellow");
        $(".fb_b_design .content").removeClass("fb_b_lime");
        $(".fb_b_design .content").removeClass("fb_b_turquoise");
        $(".fb_b_design .content").removeClass("fb_b_pink");
        $(".fb_b_design .content").removeClass("fb_b_magenta");
        $(".fb_b_design .content").removeClass("fb_b_purple");
        $(".fb_b_design .content").removeClass("fb_b_cream");
        $(".fb_b_design .content").removeClass("fb_b_chocolate");
        $(".fb_b_design .content").removeClass("fb_b_cobalt");
    });
    // for cobalt
    $(".theme_color #cobalt").click(function () {
        $(".fb_b_design .content").addClass("fb_b_cobalt");
        $(".fb_b_design .content").removeClass("fb_b_green");
        $(".fb_b_design .content").removeClass("fb_b_blue");
        $(".fb_b_design .content").removeClass("fb_b_orange");
        $(".fb_b_design .content").removeClass("fb_b_red");
        $(".fb_b_design .content").removeClass("fb_b_yellow");
        $(".fb_b_design .content").removeClass("fb_b_lime");
        $(".fb_b_design .content").removeClass("fb_b_turquoise");
        $(".fb_b_design .content").removeClass("fb_b_pink");
        $(".fb_b_design .content").removeClass("fb_b_magenta");
        $(".fb_b_design .content").removeClass("fb_b_purple");
        $(".fb_b_design .content").removeClass("fb_b_cream");
        $(".fb_b_design .content").removeClass("fb_b_chocolate");
        $(".fb_b_design .content").removeClass("fb_b_silver");
    });
});
// header color end 

