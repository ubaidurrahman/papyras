// onload pop-up style changing trigger
$(".content_list li a").click(function () {
    if (this.id == "popup_trigger") {
        $(".onload_modal .modal-body").addClass("popup_design");
    } else {
        $(".onload_modal .modal-body").removeClass("popup_design");
    }
});

// theme changing start 
$(document).ready(function () {
    // for theme 01
    $(".theme_trigger #theme_01").click(function () {
        $(".onload_modal .popup_design").empty();
        $(".onload_modal .popup_design").append("<div class='onload_modal_body modal_01 p_green p_text_light'><a href='#' class='modal_close' data-dismiss='modal'><i class='ion-close-round'></i></a><div class='modal_header'><h3>Newsletter</h3></div><div class='main_body'><h6>Stay up to date with our latest news and products.</h6><form action=''><input type='email' placeholder='Your email address'><button type='submit' data-dismiss='modal' class='modal_button'>Subscribe</button></form><p>Your email is safe with us, we don't spam.</p></div></div>");
    });
    // for theme 02
    $(".theme_trigger #theme_02").click(function () {
        $(".onload_modal .popup_design").empty();
        $(".onload_modal .popup_design").append("<div class='onload_modal_body modal_02 p_green p_text_light'><a href='#' class= 'modal_close' data-dismiss='modal'><i class='ion-close-round'></i></a><div class='row'><div class='col-sm-6 pr-sm-0'><div class='modal_header_with_img'><h3>Newsletter</h3></div></div><div class='col-sm-6 pl-sm-0'><div class='main_body'><h6>Stay up to date with our latest news and products.</h6><form action=''><input type='email' placeholder='Your email address'><button type='submit' data-dismiss='modal' class='modal_button'>Subscribe</button></form><p>Your email is safe with us, we don't spam.</p></div></div></div></div>");
    });
    // for theme 03
    $(".theme_trigger #theme_03").click(function () {
        $(".onload_modal .popup_design").empty();
        $(".onload_modal .popup_design").append("<div class='onload_modal_body modal_03 p_green p_text_light'><a href='#' class='modal_close' data-dismiss='modal' ><i class='ion-close-round'></i></a><div class='row'><div class='col-sm-6 pr-sm-0'><div class='modal_img_shape'><img src='images/popup_bg/1.png' alt=''></div></div><div class='col-sm-6 pl-sm-0'><div class='main_body'><h4 class='header_text'>Early Stage Startup?</h4><h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, fugiat. Iste dolorem, quae tempore</h6><a href='#' data-dismiss='modal' class='modal_button'>Check out this offer</a></div></div></div></div>");
    });
    // for theme 04
    $(".theme_trigger #theme_04").click(function () {
        $(".onload_modal .popup_design").empty();
        $(".onload_modal .popup_design").append("<div class='onload_modal_body modal_04 p_green p_text_light'><a href='#' class='modal_close' data-dismiss='modal'><i class='ion-close-round'></i></a><div class='modal_header'><h3>Newsletter</h3></div><div class='main_body'><h6>Stay up to date with our latest news and products.</h6><form action=''><input type='email' placeholder='Your email address'><button type='submit' data-dismiss='modal' class='modal_button'>Subscribe</button></form><p>Your email is safe with us, we don't spam.</p></div></div>");
    });
    // for theme 05
    $(".theme_trigger #theme_05").click(function () {
        $(".onload_modal .popup_design").empty();
        $(".onload_modal .popup_design").append("<div class='onload_modal_body modal_05 p_green p_text_light'><a href='#' class= 'modal_close' data-dismiss='modal'><i class='ion-close-round'></i></a><div class='row'><div class='col-sm-6 pr-sm-0'><div class='modal_header_with_img'><h3>Newsletter</h3></div></div><div class='col-sm-6 pl-sm-0'><div class='main_body'><h6>Stay up to date with our latest news and products.</h6><form action=''><input type='email' placeholder='Your email address'><button type='submit' data-dismiss='modal' class='modal_button'>Subscribe</button></form><p>Your email is safe with us, we don't spam.</p></div></div></div></div>");
    });
    // for theme 06
    $(".theme_trigger #theme_06").click(function () {
        $(".onload_modal .popup_design").empty();
        $(".onload_modal .popup_design").append("<div class='onload_modal_body modal_06 p_green p_text_light'><a href='#' class='modal_close' data-dismiss='modal' ><i class='ion-close-round'></i></a><div class='row'><div class='col-sm-6 pr-sm-0'><div class='modal_img_shape'><img src='images/popup_bg/1.png' alt=''></div></div><div class='col-sm-6 pl-sm-0'><div class='main_body'><h4 class='header_text'>Early Stage Startup?</h4><h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, fugiat. Iste dolorem, quae tempore</h6><a href='#' data-dismiss='modal' class='modal_button'>Check out this offer</a></div></div></div></div>");
    });
});
// theme changing end



// theme color start
$(document).ready(function () {
    // for green
    $(".theme_color #green").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_green");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_blue");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_orange");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_red");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_yellow");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_lime");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_turquoise");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_pink");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_magenta");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_purple");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cream");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_chocolate");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_silver");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cobalt");
    });
    // for blue
    $(".theme_color #blue").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_blue");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_green");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_orange");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_red");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_yellow");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_lime");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_turquoise");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_pink");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_magenta");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_purple");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cream");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_chocolate");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_silver");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cobalt");
    });
    // for orange
    $(".theme_color #orange").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_orange");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_green");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_blue");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_red");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_yellow");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_lime");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_turquoise");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_pink");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_magenta");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_purple");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cream");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_chocolate");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_silver");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cobalt");
    });
    // for red
    $(".theme_color #red").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_red");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_green");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_blue");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_orange");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_yellow");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_lime");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_turquoise");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_pink");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_magenta");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_purple");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cream");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_chocolate");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_silver");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cobalt");
    });
    // for yellow
    $(".theme_color #yellow").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_yellow");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_green");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_blue");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_orange");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_red");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_lime");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_turquoise");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_pink");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_magenta");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_purple");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cream");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_chocolate");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_silver");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cobalt");
    });
    // for lime
    $(".theme_color #lime").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_lime");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_green");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_blue");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_orange");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_red");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_yellow");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_turquoise");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_pink");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_magenta");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_purple");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cream");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_chocolate");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_silver");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cobalt");
    });
    // for turquoise
    $(".theme_color #turquoise").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_turquoise");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_green");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_blue");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_orange");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_red");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_yellow");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_lime");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_pink");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_magenta");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_purple");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cream");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_chocolate");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_silver");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cobalt");
    });
    // for pink
    $(".theme_color #pink").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_pink");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_green");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_blue");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_orange");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_red");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_yellow");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_lime");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_turquoise");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_magenta");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_purple");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cream");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_chocolate");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_silver");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cobalt");
    });
    // for magenta
    $(".theme_color #magenta").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_magenta");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_green");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_blue");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_orange");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_red");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_yellow");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_lime");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_turquoise");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_pink");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_purple");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cream");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_chocolate");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_silver");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cobalt");
    });
    // for purple
    $(".theme_color #purple").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_purple");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_green");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_blue");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_orange");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_red");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_yellow");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_lime");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_turquoise");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_pink");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_magenta");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cream");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_chocolate");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_silver");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cobalt");
    });
    // for cream
    $(".theme_color #cream").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_cream");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_green");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_blue");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_orange");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_red");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_yellow");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_lime");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_turquoise");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_pink");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_magenta");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_purple");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_chocolate");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_silver");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cobalt");
    });
    // for chocolate
    $(".theme_color #chocolate").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_chocolate");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_green");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_blue");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_orange");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_red");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_yellow");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_lime");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_turquoise");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_pink");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_magenta");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_purple");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cream");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_silver");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cobalt");
    });
    // for silver
    $(".theme_color #silver").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_silver");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_green");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_blue");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_orange");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_red");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_yellow");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_lime");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_turquoise");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_pink");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_magenta");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_purple");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cream");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_chocolate");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cobalt");
    });
    // for cobalt
    $(".theme_color #cobalt").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_cobalt");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_green");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_blue");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_orange");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_red");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_yellow");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_lime");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_turquoise");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_pink");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_magenta");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_purple");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_cream");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_chocolate");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_silver");
    });
});
// theme color end 


// text color start 
$(document).ready(function () {
    // for text light 
    $(".text_color #text_light").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_text_light");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_text_dark");
    });
    // for text dark 
    $(".text_color #text_dark").click(function () {
        $(".onload_modal .popup_design .onload_modal_body").addClass("p_text_dark");
        $(".onload_modal .popup_design .onload_modal_body").removeClass("p_text_light");
    });
});