// theme_btn style changing trigger
$(".content_list li a").click(function () {
    if (this.id == "theme_btn_trigger") {
        $(".theme_btn").addClass("theme_btn_design");
        $(".theme_btn").removeClass("d-none");
    } else {
        $(".theme_btn").removeClass("theme_btn_design");
        $(".theme_btn").addClass("d-none");
    }
});

// btn position start 
$(document).ready(function () {
    // for btn left
    $(".btn_position_trigger #btn_left_trigger").click(function () {
        $(".theme_btn_design").addClass("text-left");
        $(".theme_btn_design").removeClass("text-center");
        $(".theme_btn_design").removeClass("text-right");
    });
    // for btn center
    $(".btn_position_trigger #btn_center_trigger").click(function () {
        $(".theme_btn_design").addClass("text-center");
        $(".theme_btn_design").removeClass("text-left");
        $(".theme_btn_design").removeClass("text-right");
    });
    // for btn right
    $(".btn_position_trigger #btn_right_trigger").click(function () {
        $(".theme_btn_design").addClass("text-right");
        $(".theme_btn_design").removeClass("text-center");
        $(".theme_btn_design").removeClass("text-left");
    });
});
// btn position end


// btn changing start 
$(document).ready(function () {
    // for btn 01
    $(".theme_trigger #theme_01").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_01");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_02");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_03");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_04");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_05");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_06");
    });
    // for btn 02
    $(".theme_trigger #theme_02").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_02");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_01");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_03");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_04");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_05");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_06");
    });
    // for btn 03
    $(".theme_trigger #theme_03").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_03");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_01");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_02");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_04");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_05");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_06");
    });
    // for btn 04
    $(".theme_trigger #theme_04").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_04");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_01");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_02");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_03");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_05");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_06");
    });
    // for btn 05
    $(".theme_trigger #theme_05").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_05");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_01");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_02");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_03");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_04");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_06");
    });
    // for btn 06
    $(".theme_trigger #theme_06").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_06");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_01");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_02");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_03");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_04");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_05");
    });
});
// btn changing end


// theme_btn color start
$(document).ready(function () {
    // for green
    $(".theme_color #green").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_green");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_blue");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_orange");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_red");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_yellow");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_lime");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_turquoise");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_pink");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_magenta");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_purple");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cream");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_chocolate");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_silver");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cobalt");
    });
    // for blue
    $(".theme_color #blue").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_blue");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_green");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_orange");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_red");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_yellow");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_lime");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_turquoise");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_pink");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_magenta");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_purple");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cream");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_chocolate");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_silver");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cobalt");
    });
    // for orange
    $(".theme_color #orange").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_orange");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_green");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_blue");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_red");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_yellow");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_lime");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_turquoise");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_pink");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_magenta");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_purple");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cream");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_chocolate");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_silver");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cobalt");
    });
    // for red
    $(".theme_color #red").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_red");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_green");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_blue");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_orange");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_yellow");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_lime");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_turquoise");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_pink");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_magenta");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_purple");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cream");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_chocolate");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_silver");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cobalt");
    });
    // for yellow
    $(".theme_color #yellow").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_yellow");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_green");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_blue");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_orange");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_red");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_lime");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_turquoise");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_pink");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_magenta");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_purple");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cream");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_chocolate");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_silver");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cobalt");
    });
    // for lime
    $(".theme_color #lime").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_lime");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_green");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_blue");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_orange");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_red");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_yellow");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_turquoise");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_pink");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_magenta");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_purple");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cream");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_chocolate");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_silver");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cobalt");
    });
    // for turquoise
    $(".theme_color #turquoise").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_turquoise");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_green");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_blue");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_orange");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_red");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_yellow");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_lime");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_pink");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_magenta");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_purple");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cream");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_chocolate");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_silver");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cobalt");
    });
    // for pink
    $(".theme_color #pink").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_pink");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_green");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_blue");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_orange");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_red");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_yellow");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_lime");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_turquoise");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_magenta");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_purple");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cream");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_chocolate");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_silver");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cobalt");
    });
    // for magenta
    $(".theme_color #magenta").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_magenta");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_green");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_blue");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_orange");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_red");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_yellow");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_lime");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_turquoise");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_pink");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_purple");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cream");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_chocolate");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_silver");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cobalt");
    });
    // for purple
    $(".theme_color #purple").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_purple");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_green");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_blue");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_orange");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_red");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_yellow");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_lime");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_turquoise");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_pink");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_magenta");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cream");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_chocolate");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_silver");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cobalt");
    });
    // for cream
    $(".theme_color #cream").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_cream");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_green");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_blue");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_orange");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_red");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_yellow");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_lime");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_turquoise");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_pink");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_magenta");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_purple");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_chocolate");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_silver");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cobalt");
    });
    // for chocolate
    $(".theme_color #chocolate").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_chocolate");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_green");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_blue");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_orange");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_red");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_yellow");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_lime");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_turquoise");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_pink");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_magenta");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_purple");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cream");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_silver");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cobalt");
    });
    // for silver
    $(".theme_color #silver").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_silver");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_green");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_blue");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_orange");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_red");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_yellow");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_lime");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_turquoise");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_pink");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_magenta");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_purple");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cream");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_chocolate");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cobalt");
    });
    // for cobalt
    $(".theme_color #cobalt").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_cobalt");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_green");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_blue");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_orange");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_red");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_yellow");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_lime");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_turquoise");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_pink");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_magenta");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_purple");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_cream");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_chocolate");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_silver");
    });
});
// theme_btn color end 


// text color start 
$(document).ready(function () {
    // for text light 
    $(".text_color #text_light").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_text_light");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_text_dark");
    });
    // for text dark 
    $(".text_color #text_dark").click(function () {
        $(".theme_btn_design .theme_btn_link").addClass("btn_text_dark");
        $(".theme_btn_design .theme_btn_link").removeClass("btn_text_light");
    });
});