// call_to_action style changing trigger
$(".content_list li a").click(function () {
    if (this.id == "call_to_action_trigger") {
        $(".call_to_action").addClass("cta_design");
        $(".call_to_action").removeClass("d-none");
        $(".email_collector").addClass("d-none");
    } else {
        $(".call_to_action").removeClass("cta_design");
        $(".call_to_action").addClass("d-none");
        $(".email_collector").removeClass("d-none");
    }
});

// theme changing start 
$(document).ready(function () {
    // for theme 01
    $(".theme_trigger #theme_01").click(function () {
        $(".cta_design").empty();
        $(".cta_design").append("<div class='content theme_01'><form action=''><div class='row'><div class='col-sm-6'><div class='form-group'><label for=''>Name</label><input type='text' placeholder='Enter your name'></div><div class='form-group'><label for=''>Email</label><input type='email' placeholder='Your email address'></div><div class='row'><div class='col-6 pr-1'><div class='form-group'><label for=''>Age</label><input type='text' placeholder='Date of birth'></div></div><div class='col-6 pl-1'><div class='form-group'><label for=''>Phone</label><input type='text' placeholder='Phone no'></div></div></div></div><div class='col-sm-6'><div class='image'><img src='images/call_to_action/1.png' alt=''></div><button>Download</button></div></div></form></div>");
    });
    // for theme 02
    $(".theme_trigger #theme_02").click(function () {
        $(".cta_design").empty();
        $(".cta_design").append("<div class='content theme_02'><div class='row'><div class='col-sm-6'><h3>Download our</h3><h5>latest ebook</h5><a href='#'>Download</a></div></div></div>");
    });
    // for theme 03
    $(".theme_trigger #theme_03").click(function () {
        $(".cta_design").empty();
        $(".cta_design").append("<div class='content theme_03'><h2> Get 50 % Off</h2><h4>on subscription</h4><p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Laboriosam, voluptate deserunt tenetur</p><a href='#'>Click here</a></div>");
    });
    // for theme 04
    $(".theme_trigger #theme_04").click(function () {
        $(".cta_design").empty();
        $(".cta_design").append("<div class='content theme_04'><div class='row'><div class='col-sm-7'><h4>Get touch with us and</h4><h5>Download free ebook</h5></div><div class='col-sm-5'><form action=''><div class='form-group'><input type='email' placeholder='Enter your email address'></div><button>Download</button></form></div></div></div>");
    });
    // for theme 05
    $(".theme_trigger #theme_05").click(function () {
        $(".cta_design").empty();
        $(".cta_design").append("<div class='content theme_05'><div class='row'><div class='col-sm-7'><h2>Get 50% Off</h2><h4>on subscription</h4><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></div><div class='col-sm-5'><div class='d-flex justify-content-center align-items-center h-100'><a href='#'>Click here</a></div></div></div></div>");
    });
    // for theme 06
    $(".theme_trigger #theme_06").click(function () {
        $(".cta_design").empty();
        $(".cta_design").append("<div class='content theme_06'><div class='icon'><img src='images/call_to_action/6.png' alt=''></div><form action=''><div class='form-group'><input type='text' placeholder='Enter your name'></div><div class='form-group mb-4'><input type='email' placeholder='Enter your email address'></div><button>Download</button></form></div>");
    });
});
// theme changing end